package geometry

import (
	"image"
	"image/color"

	"github.com/llgcode/draw2d/draw2dimg"
)

type ShapeType int

const (
	ConcaveType ShapeType = iota
	ConvexType
	LineType
	PointType
	CircleType
	AABType
)

type Shape interface {
	Vertex(int) Vector
	FirstVertexIndex() int
	NextVertexIndexClockwise(int) int
	NextVertexIndexAnticlockwise(int) int
	ShapeType() ShapeType
	BoundingBox() AAB
	FirstConvexComponentIndex() int
	NextConvexComponentIndex(int) int
	ConvexComponent(int) SubShape
}

func FirstVertexAngle(s Shape) float32 {
	cnt := s.BoundingBox().Centre()
	fv := s.Vertex(s.FirstVertexIndex())
	return fv.Minus(cnt).Angle()
}

func CountVertices(s Shape) int {
	i := s.FirstVertexIndex()
	start := i
	var count int
	for {
		count++
		i = s.NextVertexIndexClockwise(i)
		if i == start {
			return count
		}
	}
}

func CountComponents(s Shape) int {
	i := s.FirstConvexComponentIndex()
	start := i
	var count int
	for {
		count++
		i = s.NextConvexComponentIndex(i)
		if i == start {
			return count
		}
	}
}

//
// func snapshot(s Shape, box AAB, applybox bool) Shape {
// 	switch s.ShapeType() {
// 	case AABType:
// 		if applybox {
// 			return box
// 		}
// 		return s.BoundingBox()
// 	case CircleType:
//
// 		sbox := s.BoundingBox()
// 		rad := sbox.RadiusX()
// 		ang := FirstVertexAngle(s)
// 		cnt := sbox.Centre()
// 		if applybox {
// 			rad = box.RadiusX()
// 			if y := box.RadiusY(); y < rad {
// 				rad = y
// 			}
// 			cnt = box.Centre()
// 		}
// 		return NewCircleFromCentreRadiusAngle(cnt, rad, ang)
// 	case PointType:
// 		if applybox {
// 			return box.Centre()
// 		}
// 		return s.Vertex(s.FirstVertexIndex())
// 	case LineType:
// 		fvi := s.FirstVertexIndex()
// 		v1, v2 := s.Vertex(fvi), s.Vertex(s.NextVertexIndexClockwise(fvi))
// 		if applybox {
// 			if v1.X() < v2.X() {
// 				if v1.Y() < v2.Y() {
// 					//lb to rt
// 					return NewLineFromStartEnd(box.LB(), box.RT())
// 				} else {
// 					//lt to rb
// 					return NewLineFromStartEnd(box.LT(), box.RB())
// 				}
// 			} else {
// 				if v1.Y() < v2.Y() {
// 					//rb to lt
// 					return NewLineFromStartEnd(box.RB(), box.LT())
// 				} else {
// 					//rt to lb
// 					return NewLineFromStartEnd(box.RT(), box.LB())
// 				}
// 			}
// 		}
// 		return NewLineFromStartEnd(v1, v2)
// 	case ConcaveType:
// 		newBase := Convex{make([]float32, CountVertices(s)*2)}
// 		var oldBox AAB
// 		if applybox {
// 			oldBox = s.BoundingBox()
// 		}
// 		i := s.FirstVertexIndex()
// 		start := i
// 		var pos int
// 		for {
// 			v := s.Vertex(i)
// 			if applybox {
// 				v = v.Minus(oldBox.Centre()).Over(oldBox.Radius())
// 				v = v.Times(box.Radius()).Plus(box.Centre())
// 			}
// 			newBase.vs[pos] = v.X()
// 			newBase.vs[pos+1] = v.Y()
// 			pos += 2
// 			i = s.NextVertexIndexClockwise(i)
// 			if i == start {
// 				break
// 			}
// 		}
// 		cci := s.FirstConvexComponentIndex()
// 		startcci := cci
// 		newS := Concave{}
// 		newS.components = make([]SubShape, CountComponents(s))
// 		pos = 0
// 		for {
// 			component := s.ConvexComponent(cci)
// 			component.setBase(newBase)
// 			newS.components[pos] = component
// 			pos++
// 			cci = s.NextConvexComponentIndex(cci)
// 			if cci == startcci {
// 				break
// 			}
// 		}
// 		newS.Shape = newBase
// 		return newS
// 	default:
// 		//Convex
// 		newS := Convex{make([]float32, CountVertices(s)*2)}
// 		i := s.FirstVertexIndex()
// 		start := i
// 		var pos int
// 		var oldBox AAB
// 		if applybox {
// 			oldBox = s.BoundingBox()
// 		}
// 		for {
// 			v := s.Vertex(i)
// 			if applybox {
// 				v = v.Minus(oldBox.Centre()).Over(oldBox.Radius())
// 				v = v.Times(box.Radius()).Plus(box.Centre())
// 			}
// 			newS.vs[pos] = v.X()
// 			newS.vs[pos+1] = v.Y()
// 			pos += 2
// 			i = s.NextVertexIndexClockwise(i)
// 			if i == start {
// 				break
// 			}
// 		}
// 		return newS
// 	}
// }
//
// func SnapshotToBoundingBox(s Shape, box AAB) Shape {
// 	return snapshot(s, box, true)
// }
//
// func SnapshotToOrigin(s Shape) Shape {
// 	box := s.BoundingBox()
// 	box.setCentreX(0)
// 	box.setCentreY(0)
// 	return SnapshotToBoundingBox(s, box)
// }
//
// func Snapshot(s Shape) Shape {
// 	return snapshot(s, AABZero, false)
// }

func DrawShapesToFile(filename string, ps ...Shape) {
	// Initialize the graphic context on an RGBA image
	a := ps[0].BoundingBox()
	for i := 1; i < len(ps); i++ {
		a1 := ps[i].BoundingBox()
		if a1.L() < a.L() {
			a.setL(a1.L())
		}
		if a1.R() > a.R() {
			a.setR(a1.R())
		}
		if a1.B() < a.B() {
			a.setB(a1.B())
		}
		if a1.T() > a.T() {
			a.setT(a1.T())
		}
	}
	offset := Vector{0, 0}.Minus(a.LB())
	scale := 10
	dest := image.NewRGBA(image.Rect(0, 0, int(a.R()+offset.X())*scale, int(a.T()+offset.Y())*scale))
	gc := draw2dimg.NewGraphicContext(dest)

	// set some properties
	gc.SetStrokeColor(color.RGBA{0x44, 0x44, 0x44, 0xff})
	gc.SetLineWidth(3)
	for pi, p := range ps {
		gc.SetFillColor(color.RGBA{0x44 * uint8(pi), 0xff * uint8(pi), 0x44 * uint8(pi), 0x44})
		i := p.FirstVertexIndex()
		start := i
		v := p.Vertex(i)
		v = v.Plus(offset)
		v.setY(a.T() + offset.Y() - v.Y())
		v = v.TimesXY(float32(scale))
		for {
			if i == start {
				gc.MoveTo(float64(v.X()), float64(v.Y()))
			} else {
				gc.LineTo(float64(v.X()), float64(v.Y()))
			}
			i = p.NextVertexIndexClockwise(i)
			v = p.Vertex(i)
			v = v.Plus(offset)
			v.setY(a.T() + offset.Y() - v.Y())
			v = v.TimesXY(float32(scale))

			if i == start {
				gc.LineTo(float64(v.X()), float64(v.Y()))
				break
			}
		}
		gc.Close()
		gc.FillStroke()
	}
	// Save to file
	draw2dimg.SaveToPngFile(filename, dest)
}

func Project(s Shape, axis Vector) (min, max float32) {
	i := s.FirstVertexIndex()
	start := i
	v := s.Vertex(i)
	min = v.Dot(axis)
	max = min
	i = s.NextVertexIndexClockwise(i)
	for {
		v = s.Vertex(i)
		p := v.Dot(axis)
		if p < min {
			min = p
		} else if p > max {
			max = p
		}
		i = s.NextVertexIndexClockwise(i)
		if i == start {
			break
		}
	}
	return
}

func AxisOfMinimumWidth(s Shape) (axis Vector) {
	ai := s.FirstVertexIndex()
	start := ai
	a := s.Vertex(ai)
	bi := ai
	b := a
	var w float32
	for {
		ai, a = bi, b
		bi = s.NextVertexIndexClockwise(ai)
		if start == bi {
			break
		}
		b = s.Vertex(bi)
		ax := b.Minus(a)
		min, max := Project(s, ax)
		width := max - min
		if width < w {
			w = width
			axis = ax
		}
	}
	return
}
