package geometry

type Convex struct{ vs []float32 } //x,y ...

//x,y...
func NewConvex(fs ...float32) Convex {
	return Convex{fs}
}
func NewConvexFromVectors(vs ...Vector) Convex {
	cv := make([]float32, len(vs)*2)
	for i, v := range vs {
		cvi := i * 2
		cv[cvi] = v.X()
		cv[cvi+1] = v.Y()
	}
	return Convex{cv}
}

func (cv Convex) setVertex(i int, v Vector) {
	i *= 2
	cv.vs[i] = v.X()
	cv.vs[i+1] = v.Y()
}
func (cv Convex) Rotate(radians float32) {
	i := cv.FirstVertexIndex()
	start := i

	for {
		v := cv.Vertex(i)
		cv.setVertex(i, v.Rotated(radians))
		i = cv.NextVertexIndexClockwise(i)
		if i == start {
			break
		}
	}
}
func (cv Convex) Translate(t Vector) {
	i := cv.FirstVertexIndex()
	start := i

	for {
		v := cv.Vertex(i)
		cv.setVertex(i, v.Plus(t))
		i = cv.NextVertexIndexClockwise(i)
		if i == start {
			break
		}
	}
}

//Shape interface

func (cv Convex) BoundingBox() AAB { return CalculateBoundingBoxOfPolygon(cv) }

// func (cv Convex) Copy() Shape {
// 	cv2 := make(Convex, len(cv))
// 	for i, v := range cv {
// 		cv2[i] = v
// 	}
// 	return cv2
// }
func (cv Convex) ConvexComponent(int) SubShape     { return NewCompleteSubShape(cv) }
func (cv Convex) FirstConvexComponentIndex() int   { return 0 }
func (cv Convex) NextConvexComponentIndex(int) int { return 0 }
func (cv Convex) FirstVertexIndex() int            { return 0 }
func (cv Convex) NextVertexIndexAnticlockwise(i int) int {
	if i == 0 {
		return (len(cv.vs) / 2) - 1
	}
	return i - 1
}
func (cv Convex) NextVertexIndexClockwise(i int) int {
	if i == (len(cv.vs)/2)-1 {
		return 0
	}
	return i + 1
}
func (cv Convex) ShapeType() ShapeType { return ConvexType }
func (cv Convex) Vertex(i int) Vector {
	i *= 2
	return Vector{cv.vs[i], cv.vs[i+1]}
}
