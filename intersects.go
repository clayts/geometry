package geometry

func Intersects(s1, s2 Shape) bool {
	switch s1.ShapeType() {
	case AABType:
		switch s2.ShapeType() {
		case AABType:
			//AAB vs AAB
			return intersectsAABVSAAB(s1, s2)
		case CircleType:
			//AAB vs Circle
			return intersectsCircleVSConvex(s2, s1)
		case PointType:
			//AAB vs Point
			return intersectsAABVSPoint(s1, s2)
		case LineType:
			//AAB vs Line
			return intersectsConvexVSConvex(s2, s1)
		case ConvexType:
			//AAB vs Convex
			return intersectsConvexVSConvex(s1, s2)
		default:
			//AAB vs Concave
			return intersectsConvexVSConcave(s1, s2)
		}
	case CircleType:
		switch s2.ShapeType() {
		case AABType:
			//Reverse
			return intersectsCircleVSConvex(s1, s2)
		case CircleType:
			//Circle vs Circle
			return intersectsCircleVSCircle(s1, s2)
		case PointType:
			//Circle vs Point
			return intersectsCircleVSPoint(s1, s2)
		case LineType:
			//Circle vs Line
			return intersectsCircleVSConvex(s1, s2)
		case ConvexType:
			//Circle vs Convex
			return intersectsCircleVSConvex(s1, s2)
		default:
			//Circle vs Concave
			return intersectsCircleVSConcave(s1, s2)
		}
	case PointType:
		switch s2.ShapeType() {
		case AABType:
			//Reverse
			return intersectsAABVSPoint(s2, s1)
		case CircleType:
			//Reverse
			return intersectsCircleVSPoint(s2, s1)
		case PointType:
			//Point vs Point
			return false
		case LineType:
			//Point vs Line
			return false
		case ConvexType:
			//Point vs Convex
			return intersectsConvexVSConvex(s1, s2)
		default:
			//Point vs Concave
			return intersectsPointVSConcave(s1, s2)
		}
	case LineType:
		switch s2.ShapeType() {
		case AABType:
			//Reverse
			return intersectsConvexVSConvex(s1, s2)
		case CircleType:
			//Reverse
			return intersectsCircleVSConvex(s2, s1)
		case PointType:
			//Reverse
			return false
		case LineType:
			//Line vs Line
			return false
		case ConvexType:
			//Line vs Convex
			return intersectsConvexVSConvex(s1, s2)
		default:
			//Line vs Concave
			return intersectsLineVSConcave(s1, s2)
		}
	case ConvexType:
		switch s2.ShapeType() {
		case AABType:
			//Reverse
			return intersectsConvexVSConvex(s1, s2)
		case CircleType:
			//Reverse
			return intersectsCircleVSConvex(s2, s1)
		case PointType:
			//Reverse
			return intersectsConvexVSConvex(s1, s2)
		case LineType:
			//Reverse
			return intersectsConvexVSConvex(s2, s1)
		case ConvexType:
			//Convex vs Convex
			return intersectsConvexVSConvex(s1, s2)
		default:
			//Convex vs Concave
			return intersectsConvexVSConcave(s1, s2)
		}
	}
	//ConcaveType
	switch s2.ShapeType() {
	case AABType:
		//Reverse
		return intersectsConvexVSConcave(s2, s1)
	case CircleType:
		//Reverse
		return intersectsCircleVSConcave(s2, s1)
	case PointType:
		//Reverse
		return intersectsPointVSConcave(s2, s1)
	case LineType:
		//Reverse
		return intersectsLineVSConcave(s2, s1)
	case ConvexType:
		//Reverse
		return intersectsConvexVSConcave(s2, s1)
	}
	//Concave vs Concave
	return intersectsConcaveVSConcave(s1, s2)
}

func intersectsAABVSAAB(s1, s2 Shape) bool {
	a, b := s1.BoundingBox(), s2.BoundingBox()
	return a.L() < b.R() && a.R() > b.L() && a.B() < b.T() && a.T() > b.B()
}
func intersectsAABVSPoint(s1, s2 Shape) bool {
	a := s1.BoundingBox()
	b := s2.Vertex(s2.FirstVertexIndex())
	return a.L() < b.X() && a.R() > b.X() && a.B() < b.Y() && a.T() > b.Y()
}
func intersectsCircleVSCircle(s1, s2 Shape) bool {
	a, b := s1.BoundingBox(), s2.BoundingBox()
	return a.Centre().Minus(b.Centre()).LengthSq() < a.RadiusX()*a.RadiusY()+b.RadiusX()*b.RadiusY()
}
func intersectsCircleVSPoint(s1, s2 Shape) bool {
	a := s1.BoundingBox()
	b := s2.Vertex(s2.FirstVertexIndex())
	return a.Centre().Minus(b).LengthSq() < a.RadiusX()*a.RadiusY()
}
func intersectsCircleVSConvex(s1, s2 Shape) bool {
	psb, psbv, pst, pstv := leftVertices(s2)
	box := s1.BoundingBox()
	centre := box.Centre()
	radius := box.RadiusX()
	if pstv.Y() < centre.Y() {
		//circle is on top
		//check if c goes underneath the top side of p
		ai := pst
		a := pstv
		for {
			bi := s2.NextVertexIndexClockwise(ai)
			b := s2.Vertex(bi)
			if b.X() <= a.X() || centre.X()+radius < a.X() {
				//no more lines
				//or
				//circle is left of line, we're done
				return false
			}
			if centre.X()-radius > b.X() || (centre.Y()-radius > a.Y() && centre.Y()-radius > b.Y()) {
				//circle is right of line, go to next line
				//or
				//circle is above line, go to next line
			} else if ClockwiseOfAB(centre, a, b) || (NearestPointBetweenAB(centre, a, b).Minus(centre).LengthSq() < radius*radius) {
				//circle's center is below line
				//or
				//circle intersects line
				return true
			}
			ai, a = bi, b
		}
	} else {
		//circle is on bottom
		//check if c goes above the bottom side of p
		ai := psb
		a := psbv
		for {
			bi := s2.NextVertexIndexAnticlockwise(ai)
			b := s2.Vertex(bi)
			if b.X() <= a.X() || centre.X()+radius < a.X() {
				//no more lines
				//or
				//circle is left of line, we're done
				return false
			}
			if centre.X()-radius > b.X() || (centre.Y()+radius < a.Y() && centre.Y()+radius < b.Y()) {
				//circle is right of line, go to next line
				//or
				//circle is below line, go to next line
			} else if ClockwiseOfAB(centre, b, a) || (NearestPointBetweenAB(centre, a, b).Minus(centre).LengthSq() < radius*radius) {
				//circle's center is above line
				//or
				//circle intersects line
				return true
			}
			ai, a = bi, b
		}
	}
}
func intersectsCircleVSConcave(s1, s2 Shape) bool {
	cci := s2.FirstConvexComponentIndex()
	ccistart := cci
	for {
		if intersectsCircleVSConvex(s1, s2.ConvexComponent(cci)) {
			return true
		}
		cci = s2.NextConvexComponentIndex(cci)
		if cci == ccistart {
			break
		}
	}
	return false
}
func intersectsPointVSConcave(s1, s2 Shape) bool {
	cci := s2.FirstConvexComponentIndex()
	ccistart := cci
	for {
		if intersectsConvexVSConvex(s1, s2.ConvexComponent(cci)) {
			return true
		}
		cci = s2.NextConvexComponentIndex(cci)
		if cci == ccistart {
			break
		}
	}
	return false
}
func intersectsLineVSConcave(s1, s2 Shape) bool {
	cci := s2.FirstConvexComponentIndex()
	ccistart := cci
	for {
		if intersectsConvexVSConvex(s1, s2.ConvexComponent(cci)) {
			return true
		}
		cci = s2.NextConvexComponentIndex(cci)
		if cci == ccistart {
			break
		}
	}
	return false
}
func intersectsConvexVSConvex(s1, s2 Shape) bool {
	//get the leftmost vertex of each poly
	//see which is on top
	//go anticlockwise from the leftmost vertex on the top poly, clockwise from the bottom
	//set a line pair to starting and ending on the leftmost vertex of each poly
	//loop:
	//take the line which ends first
	//if it ends before the other one starts, set the start of the line to the end of the line, set the end of the line to the next vertex, loop
	//compare its end point with the other line and return true if it crosses, if it doesn't cross, set the start of the line whos end point we checked to that end point, and the end point to the next vertex, go to start
	s1lb, s1lbv, s1lt, s1ltv := leftVertices(s1)
	s2lb, s2lbv, s2lt, s2ltv := leftVertices(s2)
	s1l, s1lv, s2l, s2lv := s1lt, s1ltv, s2lt, s2ltv
	if s1lv.Y() < s2lv.Y() {
		//s2 is on top
		s2l = s2lb
		s2lv = s2lbv
		s1, s2 = s2, s1
		s1l, s2l = s2l, s1l
		s1lv, s2lv = s2lv, s1lv
	} else {
		//s1 is on top
		s1l = s1lb
		s1lv = s1lbv
	}
	//s1 is now on top

	//compare the bottom of s1 with the top of s2
	//a>b is from s1 each vertex is anticlockwise and is crossed by p if p.Clockwise(Ray{b,a})
	//c>d is from s2 each vertex is clockwise and is crossed by p if p.Clockwise(Ray{c,d})

	ai := s1l
	a := s1lv
	bi := ai
	b := a
	ci := s2l
	c := s2lv
	di := ci
	d := c

	//cycle through each line pair after the first
	for {
		//take the line which ends first
		if b.X() < d.X() {
			//a>b ends first
			if b.X() < c.X() {
				//if it ends before the other one starts, move it forwards, go to start
			} else if ClockwiseOfAB(b, c, d) {
				//compare its last point with the other line and return true if it crosses
				return true
			}
			//move the line with the compared point forwards, go to start
			ai, a = bi, b
			bi = s1.NextVertexIndexAnticlockwise(ai)
			b = s1.Vertex(bi)
			if b.X() <= a.X() {
				return false
			}
		} else {
			//c>d ends first
			if d.X() < a.X() {
				//if it ends before the other one starts, move it forwards, go to start
			} else if ClockwiseOfAB(d, b, a) {
				//compare its last point with the other line and return true if it crosses
				return true
			}
			//move the line with the compared point forwards, go to start
			ci, c = di, d
			di = s2.NextVertexIndexClockwise(ci)
			d = s2.Vertex(di)
			if d.X() <= c.X() {
				return false
			}
		}
	}
}
func intersectsConvexVSConcave(s1, s2 Shape) bool {
	cci := s2.FirstConvexComponentIndex()
	ccistart := cci
	for {
		if intersectsConvexVSConvex(s1, s2.ConvexComponent(cci)) {
			return true
		}
		cci = s2.NextConvexComponentIndex(cci)
		if cci == ccistart {
			break
		}
	}
	return false
}
func intersectsConcaveVSConcave(s1, s2 Shape) bool {
	cci := s1.FirstConvexComponentIndex()
	ccistart := cci
	for {
		if intersectsConvexVSConcave(s1.ConvexComponent(cci), s2) {
			return true
		}
		cci = s1.NextConvexComponentIndex(cci)
		if cci == ccistart {
			break
		}
	}
	return false
}
