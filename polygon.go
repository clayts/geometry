package geometry

func NewPolygon(fs ...float32) Shape {
	if len(fs) == 2 {
		return Vector{fs[0], fs[1]}
	} else if len(fs) == 4 {
		return NewLine(fs[0], fs[1], fs[2], fs[3])
	}
	cc := NewConcave(fs...)
	if len(cc.components) == 1 {
		return cc.Shape
	}
	return cc
}

func NewPolygonFromVectors(vs ...Vector) Shape {
	if len(vs) == 1 {
		return vs[0]
	} else if len(vs) == 2 {
		return NewLineFromStartEnd(vs[0], vs[1])
	}
	cc := NewConcaveFromVectors(vs...)
	if len(cc.components) == 1 {
		return cc.Shape
	}
	return cc
}

func NewPolygonFromLineThickness(l Line, thickness float32) Shape {
	lCent := l.Start().Plus(l.End()).OverXY(2)
	var answer Shape
	answer = NewAABFromXYRadius(0, 0, Vector{l.Length() / 2, thickness / 2})
	answer = NewTransformShape(answer,
		NewRotationTransform(
			l.Angle(),
		).
			FollowedBy(
				NewTranslationTransform(
					lCent,
				),
			),
	)
	return answer
}

func leftVertices(s Shape) (bottomI int, bottomV Vector, topI int, topV Vector) {
	if s.ShapeType() == AABType {
		return int(LB), s.Vertex(int(LB)), int(LT), s.Vertex(int(LT))
	}
	first := s.FirstVertexIndex()
	ai := first
	bi := s.NextVertexIndexClockwise(ai)
	a := s.Vertex(ai)
	b := s.Vertex(bi)
	if bi == ai {
		return ai, a, bi, b
	}
	if b.X() == a.X() && b.Y() > a.Y() {
		return ai, a, bi, b
	} else if b.X() < a.X() {
		for {
			ai, a = bi, b
			bi = s.NextVertexIndexClockwise(bi)
			b = s.Vertex(bi)
			if b.X() == a.X() {
				return ai, a, bi, b
			}
			if b.X() > a.X() {
				return ai, a, ai, a
			}
		}
	} else {
		for {
			bi = s.NextVertexIndexAnticlockwise(ai)
			b = s.Vertex(bi)
			if b.X() == a.X() {
				return bi, b, ai, a
			}
			if b.X() > a.X() {
				return ai, a, ai, a
			}
			ai, a = bi, b
		}
	}
}

func EachEdge(sh Shape, f func(l Line) bool) {
	ai := sh.FirstVertexIndex()
	fvi := ai
	a := sh.Vertex(ai)
	bi := sh.NextVertexIndexClockwise(ai)
	b := sh.Vertex(bi)
	for {
		if !f(NewLineFromStartEnd(a, b)) {
			break
		}
		ai, a = bi, b
		if ai == fvi {
			break
		}
		bi = sh.NextVertexIndexClockwise(ai)
		b = sh.Vertex(bi)
	}
}

func EachVertex(sh Shape, f func(v Vector) bool) {
	ai := sh.FirstVertexIndex()
	fvi := ai
	a := sh.Vertex(ai)
	for {
		if !f(a) {
			break
		}
		ai = sh.NextVertexIndexClockwise(ai)
		a = sh.Vertex(ai)
		if ai == fvi {
			break
		}
	}
}

func CalculateBoundingBoxOfPolygon(ss ...Shape) AAB {
	var l, b, r, t float32
	for si, s := range ss {
		i := s.FirstVertexIndex()
		start := i
		v := s.Vertex(i)
		if si == 0 {
			l = v.X()
			b = v.Y()
			r = l
			t = b
		} else {
			if v.X() < l {
				l = v.X()
			} else if v.X() > r {
				r = v.X()
			}
			if v.Y() < b {
				b = v.Y()
			} else if v.Y() > t {
				t = v.Y()
			}
		}
		for {
			i = s.NextVertexIndexClockwise(i)
			if i == start {
				break
			}
			v := s.Vertex(i)
			if v.X() < l {
				l = v.X()
			} else if v.X() > r {
				r = v.X()
			}
			if v.Y() < b {
				b = v.Y()
			} else if v.Y() > t {
				t = v.Y()
			}
		}
	}
	return NewAABFromLBRT(l, b, r, t)
}
