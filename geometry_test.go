package geometry

import (
	"fmt"
	"math"
	"math/rand"
	"testing"

	"github.com/go-gl/mathgl/mgl32"
)

func strForPaste(p Shape) string {
	var answer string
	if p.ShapeType() == CircleType {
		box := p.BoundingBox()
		c := NewCircleFromCentreRadiusAngle(box.Centre(), box.RadiusX(), 0)
		return "Circle{Point{" + fmt.Sprint(c.Centre().X()) + "," + fmt.Sprint(c.Centre().Y()) + "}, " + fmt.Sprint(c.Radius()) + "}"
	}
	i := p.FirstVertexIndex()
	start := i
	for {
		v := p.Vertex(i)
		answer += "Point{" + fmt.Sprint(v.X()) + "," + fmt.Sprint(v.Y()) + "},"
		i = p.NextVertexIndexClockwise(i)
		if i == start {
			answer = answer[:len(answer)-1]
			break
		}
	}
	return "Convex{" + answer + "}"
}

func generateFloat32(min, max float32) float32 {
	return rand.Float32()*(max-min) + min
}

func generateInt(min, max int) int {
	return rand.Intn(max-min) + min
}

func generatePolygon(locationBB AAB, size float32) Convex {
	p := Convex{}
	crcl := NewCircle(
		generateFloat32(locationBB.L(), locationBB.R()), generateFloat32(locationBB.B(), locationBB.T()),
		size, 0,
	)
	for i := generateFloat32(0.0001, 1) / 5; i < 1; i += generateFloat32(0.0001, 1) / 5 {
		v := VertexOfCircle(crcl, i)
		p.vs = append(p.vs, v.X(), v.Y())
	}
	return p
}

func generateBox(minCentre, maxCentre, minSize, maxSize Vector) AAB {
	x := generateFloat32(minCentre.X(), maxCentre.X())
	y := generateFloat32(minCentre.Y(), maxCentre.Y())
	width := generateFloat32(minSize.X(), maxSize.X())
	height := generateFloat32(minSize.Y(), maxSize.Y())
	return NewAABFromCentreRadius(Vector{x, y}, Vector{width, height})
}

func generateBoxPairs(number int, minCentre, maxCentre, minSize, maxSize Vector) (answer []struct {
	a, b         AAB
	intersection bool
}) {
	for i := 0; i < number; i++ {
		a := generateBox(minCentre, maxCentre, minSize, maxSize)
		b := generateBox(minCentre, maxCentre, minSize, maxSize)
		answer = append(answer, struct {
			a, b         AAB
			intersection bool
		}{a, b, Intersects(a, b)})
	}
	return
}

func ifNotNil(f func()) {
	if f != nil {
		f()
	}
}

func intersectsAABvsAAB(n int, start, stop, fail func()) {
	ifNotNil(stop)
	for i := 0; i < n; i++ {
		pairs := generateBoxPairs(1, Vector{-10, -10}, Vector{10, 10}, Vector{1, 1}, Vector{10, 10})
		for _, p := range pairs {
			ifNotNil(start)
			if Intersects(p.a, p.b) != p.intersection {
				fmt.Println("intersection incorrect", strForPaste(p.a), ",", strForPaste(p.b), "real intersection:", p.intersection)
				fail()
			}
			ifNotNil(stop)
		}
	}
}

func vEquals(v Vector, x, y float32) bool {
	return mgl32.FloatEqualThreshold(v.X(), x, 0.000001) && mgl32.FloatEqualThreshold(v.Y(), y, 0.000001)
}

func bEquals(b1, b2 AAB) bool {
	return vEquals(b1.LB(), b2.L(), b2.B()) && vEquals(b1.RT(), b2.R(), b2.T())
}

//==============================================================================
// func TestTransformCircles(t *testing.T) {
// 	tfm := IdentityTransform
// 	tfm = tfm.After(NewTranslationTransform(NewVector(100, 100)))
// 	tfm = tfm.After(NewScaleTransform(NewVector(10, 10)))
// 	tfm = tfm.After(NewRotationTransform(1))
// 	cbef := NewCircle(10, 10, 10, 0)
// 	caft := NewTransformShape(cbef, tfm)
// 	fmt.Println(cbef.BoundingBox(), caft.BoundingBox())
// }

func TestCircleGen(t *testing.T) {
	c := NewCircle(0.3, 0, 0.07, 0)
	tfm := NewAABToAABTransform(c.BoundingBox(), AABRadiusOne)
	i := c.FirstVertexIndex()
	start := i
	for {
		fmt.Println(tfm.Transformed(c.Vertex(i)))
		i = c.NextVertexIndexClockwise(i)
		if i == start {
			break
		}
	}

	if !bEquals(CalculateBoundingBoxOfPolygon(NewTransformShape(c, tfm)), NewAABFromLBRT(-1, -1, 1, 1)) {
		fmt.Println("tfm circ", CalculateBoundingBoxOfPolygon(NewTransformShape(c, tfm)).LB(), CalculateBoundingBoxOfPolygon(NewTransformShape(c, tfm)).RT())
		t.Fail()
	}
}

func TestAABtoAABTransform(t *testing.T) {
	from := NewAABFromLBRT(0, 0, 100, 100)
	to := NewAABFromLBRT(-1, -1, 0, 0)
	tfm := NewAABToAABTransform(from, to)

	if tfm.Transformed(NewVector(1, 1)) != NewVector(-0.99, -0.99) {
		fmt.Println("aab to aab", tfm.Transformed(NewVector(1, 1)))
		t.Fail()
	}
}

func TestCircles(t *testing.T) {
	cc := NewCircle(0, 0, 1, 0)
	ccbb := cc.BoundingBox()
	// cct := NewTransformShape(cc, NewRotationTransform(0.01))
	fmt.Println("checking", ccbb, CalculateBoundingBoxOfPolygon(cc))
	if ccbb != CalculateBoundingBoxOfPolygon(cc) {
		fmt.Println("calc circ")
		fmt.Println(CalculateBoundingBoxOfPolygon(cc))
		fmt.Println(ccbb)
		t.Fail()
	}
	tfm := NewAABToAABTransform(ccbb, AABRadiusOne)
	i := cc.FirstVertexIndex()
	start := i
	for {
		v := cc.Vertex(i)
		fmt.Println("~~~~~~", tfm.Transformed(v))
		if v.X() < ccbb.L() || v.X() > ccbb.R() || v.Y() < ccbb.B() || v.Y() > ccbb.T() {
			fmt.Println("circ bb")
			fmt.Println(v)
			fmt.Println(ccbb)
			t.Fail()
		}
		i = cc.NextVertexIndexClockwise(i)
		if i == start {
			break
		}
	}

	c := NewTransformShape(NewCircle(10, 10, 10, 10), IdentityTransform)
	c = NewTransformShape(c.Base(), c.Transform().FollowedBy(NewRotationTransform(100)))
	c = NewTransformShape(c.Base(), c.Transform().FollowedBy(NewScaleTransform(NewVector(0.1, 100))))
	c = NewTransformShape(c.Base(), c.Transform().FollowedBy(NewTranslationTransform(NewVector(0, 100))))
	realBox := c.BoundingBox()
	testBox := CalculateBoundingBoxOfPolygon(c)
	if !vEquals(realBox.Centre(), testBox.Centre().X(), testBox.Centre().Y()) {
		fmt.Println("centre")
		fmt.Println("real", realBox)
		fmt.Println("test", testBox)
		t.Fail()
	}
	if !vEquals(realBox.Radius(), testBox.Radius().X(), testBox.Radius().Y()) {
		fmt.Println("rad")
		fmt.Println("real", realBox)
		fmt.Println("test", testBox)
		t.Fail()
	}
	// c = NewTransformShape(c, NewAABToAABTransform(c.BoundingBox(), AABRadiusOne))
	// nbb := NewAABFromLBRT(1, -1, 3, 1)
	// fmt.Println(CalculateBoundingBoxOfPolygon(c, nbb))
	// if nbb.BoundingBox() != c.BoundingBox() {
	// 	fmt.Println("bb")
	// 	t.Fail()
	// }
}

func TestTransform(t *testing.T) {
	tr := NewRotationTransform(101)
	tr2 := NewTranslationTransform(Vector{12, 13})
	tr3 := NewTranslationTransform(Vector{-12, -13})
	tr4 := NewRotationTransform(-101)
	if v := tr.FollowedBy(tr2).FollowedBy(tr3).FollowedBy(tr4).Transformed(NewVector(5, 5)); v != NewVector(5, 5) {
		fmt.Println(v)
		t.Fail()
	}
	if v := tr.FollowedBy(tr2).FollowedBy(tr4).FollowedBy(tr3).Transformed(NewVector(5, 5)); v == NewVector(5, 5) {
		fmt.Println(v)
		t.Fail()
	}
	if v := NewRotationTransform(math.Pi / 2).Transformed(NewVector(0, 10)); vEquals(v, -10, 0) {
		fmt.Println(v)
		t.Fail()
	}
	if trs := tr.FollowedBy(tr2).FollowedBy(tr4).FollowedBy(tr2).FollowedBy(tr).FollowedBy(tr4).FollowedBy(tr2).FollowedBy(tr).FollowedBy(tr).FollowedBy(tr4.Inverse()); !vEquals(trs.Scale(), 1, 1) {
		fmt.Println(trs.Scale())
		t.Fail()
	} else if trs = trs.FollowedBy(NewScaleTransform(Vector{10, 10})); !vEquals(trs.Scale(), 10, 10) {
		fmt.Println(trs.Scale())
		t.Fail()
	}

	box1 := NewAABFromLBRT(0, 0, 1, 1)
	box2 := NewAABFromLBRT(10, 10, 11, 11)
	tr5 := NewAABToAABTransform(box1, box2)
	if tr5.Transformed(box1.LB()) != box2.LB() {
		fmt.Println("only translation")
		fmt.Println(tr5.Transformed(box1.LB()))
		fmt.Println(tr5.Transformed(box1.LT()))
		fmt.Println(tr5.Transformed(box1.RT()))
		fmt.Println(tr5.Transformed(box1.RB()))
		fmt.Println(tr5)
		t.Fail()
	}

	box3 := NewAABFromLBRT(-1, -1, 1, 1)
	box4 := NewAABFromLBRT(-3, -3, 3, 3)
	tr6 := NewAABToAABTransform(box3, box4)
	if tr6.Transformed(box3.LB()) != box4.LB() {
		fmt.Println("only scale")
		fmt.Println(tr6.Transformed(box3.LB()))
		fmt.Println(tr6.Transformed(box3.LT()))
		fmt.Println(tr6.Transformed(box3.RT()))
		fmt.Println(tr6.Transformed(box3.RB()))
		fmt.Println(tr6)
		t.Fail()
	}

	box5 := NewAABFromLBRT(0, 0, 1, 1)
	box6 := NewAABFromLBRT(-30, -30, -20, -20)
	tr7 := NewAABToAABTransform(box5, box6)
	if tr7.Transformed(box5.LB()) != box6.LB() {
		fmt.Println("both")
		fmt.Println(tr7.Transformed(box5.LB()))
		fmt.Println(tr7.Transformed(box5.LT()))
		fmt.Println(tr7.Transformed(box5.RT()))
		fmt.Println(tr7.Transformed(box5.RB()))
		fmt.Println(tr7)
		t.Fail()
	}

	box7 := NewTransformShape(
		NewAABFromLBRT(100, 100, 101, 101),
		NewTranslationTransform(NewVector(-100, -100)),
	)
	if box7.BoundingBox().Centre() != NewVector(0.5, 0.5) {
		fmt.Println("translate 1", box7.BoundingBox().Centre())
		t.Fail()
	}

	preBox8 := NewAABFromLBRT(-50, -50, 50, 50)
	box8 := NewTransformShape(
		preBox8,
		NewScaleTransform(NewVector(1.0/100.0, 1.0/100.0)),
	)
	if box8.BoundingBox().Centre() != NewVector(0, 0) || box8.BoundingBox().Radius() != NewVector(0.5, 0.5) {
		fmt.Println("scale 1")
		fmt.Println("Centre:", box8.BoundingBox().Centre())
		fmt.Println("Was originally:", preBox8.Centre())
		fmt.Println("Should be:", NewAAB(0, 0, 0.5, 0.5).Centre())
		fmt.Println("Radius:", box8.BoundingBox().Radius())
		fmt.Println("Was originally:", preBox8.Radius())
		fmt.Println("Should be:", NewAAB(0, 0, 0.5, 0.5).Radius())
		t.Fail()
	}

	preBox9 := NewAABFromLBRT(-50, -50, 50, 50)
	box9 := NewTransformShape(
		preBox9,
		NewScaleTransform(NewVector(1.0/100.0, 1.0/100.0)).FollowedBy(NewTranslationTransform(NewVector(1000, 1000))),
	)
	if box9.BoundingBox().Centre() != NewVector(1000, 1000) || box9.BoundingBox().Radius() != NewVector(0.5, 0.5) {
		fmt.Println("scale 1")
		fmt.Println("Centre:", box9.BoundingBox().Centre())
		fmt.Println("Was originally:", preBox9.Centre())
		fmt.Println("Should be:", NewAAB(0, 0, 0.5, 0.5).Centre())
		fmt.Println("Radius:", box9.BoundingBox().Radius())
		fmt.Println("Was originally:", preBox9.Radius())
		fmt.Println("Should be:", NewAAB(0, 0, 0.5, 0.5).Radius())
		t.Fail()
	}
	//
	// cam := NewAABFromLBRT(-2, -2, 2, 2)
	// camt := NewOrthographicTransform(cam)
	// pq := NewTransformShape(AABUnit, camt.Inverse()).BoundingBox()
	// if pq != cam {
	// 	fmt.Println("cam:", cam)
	// 	fmt.Println("camt:", camt)
	// 	fmt.Println("pq:", pq)
	// 	t.Fail()
	// }
}

func TestIntersectsAABvsAAB(t *testing.T) {
	intersectsAABvsAAB(1000, nil, nil, t.Fail)
}

func BenchmarkIntersectsAABvsAAB(b *testing.B) {
	intersectsAABvsAAB(b.N, b.StartTimer, b.StopTimer, b.Fail)
}

var intersection bool

func intersectsConvexalvsConvexalTrue(n int, start, stop, fail func()) {
	ifNotNil(stop)
	area := AABZero
	for i := 0; i < n; i++ {
		a := generatePolygon(area, rand.Float32()*100)
		b := generatePolygon(area, rand.Float32()*100)
		intersection = false
		ifNotNil(start)
		if Intersects(a, b) {
			intersection = true
		}
		ifNotNil(stop)
		if intersection == false {
			fmt.Println("incorrectly false:", strForPaste(a), ",", strForPaste(b))
			fail()
		}
		// fmt.Println("intersection:", isects, a, b)
	}
}

func intersectsConvexalvsConvexalFalse(n int, start, stop, fail func()) {
	ifNotNil(stop)
	areaA := AABZero
	areaB := NewAABFromLBRT(100, 100, 100, 100)
	for i := 0; i < n; i++ {
		a := generatePolygon(areaA, rand.Float32()*10)
		b := generatePolygon(areaB, rand.Float32()*10)
		if generateFloat32(0, 1) < 0.5 {
			a, b = b, a
		}
		intersection = false
		ifNotNil(start)
		if Intersects(a, b) {
			intersection = true
		}
		ifNotNil(stop)
		if intersection == true {
			fail()
		}
		// fmt.Println("intersection:", isects, a, b)
	}
}

func TestIntersectsConvexalvsConvexalTrue(t *testing.T) {
	intersectsConvexalvsConvexalTrue(100000, nil, nil, t.Fail)
}

func BenchmarkIntersectsConvexalvsConvexalTrue(b *testing.B) {
	intersectsConvexalvsConvexalTrue(b.N, b.StartTimer, b.StopTimer, b.Fail)
}

func TestIntersectsConvexalvsConvexalFalse(t *testing.T) {
	intersectsConvexalvsConvexalFalse(100000, nil, nil, t.Fail)
}

func BenchmarkIntersectsConvexalvsConvexalFalse(b *testing.B) {
	intersectsConvexalvsConvexalFalse(b.N, b.StartTimer, b.StopTimer, b.Fail)
}
