package geometry

import (
	"math"

	"github.com/go-gl/mathgl/mgl32"
)

var Origin = Vector{0, 0}

type Vector [2]float32                           //x,y
func NewVector(x, y float32) Vector              { return Vector{x, y} }
func NewVectorFromMGL32Vec2(v mgl32.Vec2) Vector { return Vector(v) }

func (v Vector) X() float32                   { return v[0] }
func (v Vector) Y() float32                   { return v[1] }
func (v *Vector) setX(x float32)              { v[0] = x }
func (v *Vector) setY(y float32)              { v[1] = y }
func (v *Vector) setXY(x, y float32)          { v[0], v[1] = x, y }
func (v *Vector) set(v2 Vector)               { v[0], v[1] = v2[0], v2[1] }
func (v Vector) Plus(v2 Vector) Vector        { return Vector{v[0] + v2[0], v[1] + v2[1]} }
func (v Vector) PlusX(f float32) Vector       { return Vector{v[0] + f, v[1]} }
func (v Vector) PlusY(f float32) Vector       { return Vector{v[0], v[1] + f} }
func (v Vector) PlusXY(f float32) Vector      { return Vector{v[0] + f, v[1] + f} }
func (v Vector) Minus(v2 Vector) Vector       { return Vector{v[0] - v2[0], v[1] - v2[1]} }
func (v Vector) MinusX(f float32) Vector      { return Vector{v[0] - f, v[1]} }
func (v Vector) MinusY(f float32) Vector      { return Vector{v[0], v[1] - f} }
func (v Vector) MinusXY(f float32) Vector     { return Vector{v[0] - f, v[1] - f} }
func (v Vector) PlusXMinusY(v2 Vector) Vector { return Vector{v[0] + v2[0], v[1] - v2[1]} }
func (v Vector) MinusXPlusY(v2 Vector) Vector { return Vector{v[0] - v2[0], v[1] + v2[1]} }
func (v Vector) LengthSq() float32            { return v[0]*v[0] + v[1]*v[1] }
func (v Vector) Length() float32              { return Sqrt(v.LengthSq()) }
func (v Vector) Dot(v2 Vector) float32        { return v[0]*v2[0] + v[1]*v2[1] }
func (v Vector) Cross(v2 Vector) float32      { return v[0]*v2[1] - v[1]*v2[0] }
func (v Vector) Times(v2 Vector) Vector       { return Vector{v[0] * v2[0], v[1] * v2[1]} }
func (v Vector) TimesX(f float32) Vector      { return Vector{v[0] * f, v[1]} }
func (v Vector) TimesY(f float32) Vector      { return Vector{v[0], v[1] * f} }
func (v Vector) TimesXY(f float32) Vector     { return Vector{v[0] * f, v[1] * f} }
func (v Vector) Over(v2 Vector) Vector        { return Vector{v[0] / v2[0], v[1] / v2[1]} }
func (v Vector) OverX(f float32) Vector       { return Vector{v[0] / f, v[1]} }
func (v Vector) OverY(f float32) Vector       { return Vector{v[0], v[1] / f} }
func (v Vector) OverXY(f float32) Vector      { return Vector{v[0] / f, v[1] / f} }
func (v Vector) Rotated(radians float32) Vector {
	s, c := sincos(radians)
	return Vector{v.X()*c - v.Y()*s, v.X()*s + v.Y()*c}
}
func (v *Vector) Rotate(radians float32) { v.set(v.Rotated(radians)) }
func (v *Vector) Translate(t Vector) {
	v.set(v.Plus(t))
}
func (v Vector) Opposite() Vector { return Vector{-v[0], -v[1]} }

//Shape interface
func (v Vector) BoundingBox() AAB { return NewAABFromCentreRxRy(v, 0, 0) }

// func (v Vector) Copy() Shape                          { return Vector{v[0], v[1]} }
func (v Vector) ConvexComponent(int) SubShape         { return SubShape{v, 0, 0} }
func (v Vector) FirstConvexComponentIndex() int       { return 0 }
func (v Vector) NextConvexComponentIndex(int) int     { return 0 }
func (v Vector) FirstVertexIndex() int                { return 0 }
func (v Vector) NextVertexIndexAnticlockwise(int) int { return 0 }
func (v Vector) NextVertexIndexClockwise(int) int     { return 0 }
func (v Vector) ShapeType() ShapeType                 { return PointType }
func (v Vector) Vertex(int) Vector                    { return v }
func (v Vector) Angle() float32                       { return float32(math.Atan2(float64(v.Y()), float64(v.X()))) }
