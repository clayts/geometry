package geometry

type TransformShape struct {
	b Shape
	t Transform
}

func NewTransformShape(s Shape, t Transform) TransformShape {
	if ts, ok := s.(TransformShape); ok {
		return TransformShape{ts.Base(), ts.Transform().FollowedBy(t)}
	}
	return TransformShape{s, t}
}

func (p TransformShape) Transform() Transform { return p.t }
func (p TransformShape) Base() Shape          { return p.b }

func (p TransformShape) Vertex(i int) Vector {
	v := p.b.Vertex(i)
	postV := p.t.Transformed(v)
	// fmt.Println("transforming", v, "to", postV)
	return postV
}

func (p TransformShape) BoundingBox() AAB {
	switch p.ShapeType() {
	case CircleType, AABType:
		box := p.b.BoundingBox()
		return NewAABFromCentreRadius(
			p.t.Transformed(box.Centre()),
			box.Radius().Times(p.t.Scale()),
		)
	default:
		return CalculateBoundingBoxOfPolygon(p)
	}
}

func (p TransformShape) ConvexComponent(i int) SubShape {
	return NewCompleteSubShape(
		TransformShape{
			p.b.ConvexComponent(i),
			p.t,
		},
	)
}

func (p TransformShape) FirstConvexComponentIndex() int {
	return p.b.FirstConvexComponentIndex()
}

func (p TransformShape) NextConvexComponentIndex(i int) int {
	return p.b.NextConvexComponentIndex(i)
}

func (p TransformShape) FirstVertexIndex() int {
	return p.b.FirstVertexIndex()
}

func (p TransformShape) NextVertexIndexAnticlockwise(i int) int {
	return p.b.NextVertexIndexAnticlockwise(i)
}

func (p TransformShape) NextVertexIndexClockwise(i int) int {
	return p.b.NextVertexIndexClockwise(i)
}

func (p TransformShape) ShapeType() ShapeType {
	if st := p.b.ShapeType(); st == CircleType &&
		p.t.x.LengthSq() != p.t.y.LengthSq() {
		return ConvexType
	} else if st == AABType && p.t.IncludesRotation() {
		return ConvexType
	}
	return p.b.ShapeType()
}
