package geometry

func Contains(s1, s2 Shape) bool {
	switch s1.ShapeType() {
	case AABType:
		switch s2.ShapeType() {
		case AABType:
			//AAB vs AAB
			return containsAABVSAAB(s1, s2)
		case CircleType:
			//AAB vs Circle
			return containsAABVSCircle(s1, s2)
		case PointType:
			//AAB vs Point
			return containsAABVSPoint(s1, s2)
		case LineType:
			//AAB vs Line
			return containsAABVSLine(s1, s2)
		case ConvexType:
			//AAB vs Convex
			return containsAABVSConvex(s1, s2)
		default:
			//AAB vs Concave
			return containsAABVSConcave(s1, s2)
		}
	case CircleType:
		switch s2.ShapeType() {
		case AABType:
			//Circle vs AAB
			return containsCircleVSAAB(s1, s2)
		case CircleType:
			//Circle vs Circle
			return containsCircleVSCircle(s1, s2)
		case PointType:
			//Circle vs Point
			return containsCircleVSPoint(s1, s2)
		case LineType:
			//Circle vs Line
			return containsCircleVSLine(s1, s2)
		case ConvexType:
			//Circle vs Convex
			return containsCircleVSConvex(s1, s2)
		default:
			//Circle vs Concave
			return containsCircleVSConcave(s1, s2)
		}
	case PointType:
		return false
	case LineType:
		return false
	case ConvexType:
		switch s2.ShapeType() {
		case AABType:
			//Convex vs AAB
			return containsConvexVSAAB(s1, s2)
		case CircleType:
			//Convex vs Circle
			return containsConvexVSCircle(s1, s2)
		case PointType:
			//Convex vs Point
			return containsConvexVSPoint(s1, s2)
		case LineType:
			//Convex vs Line
			return containsConvexVSLine(s1, s2)
		case ConvexType:
			//Convex vs Convex
			return containsConvexVSConvex(s1, s2)
		default:
			//Convex vs Concave
			return containsConvexVSConcave(s1, s2)
		}
	}
	//ConcaveType
	panic("Cannot test ConcaveType for containment")
}

func containsAABVSAAB(s1, s2 Shape) bool {
	a, b := s1.BoundingBox(), s2.BoundingBox()
	return a.L() <= b.L() && a.R() >= b.R() && a.B() <= b.B() && a.T() >= b.T()
}
func containsAABVSCircle(s1, s2 Shape) bool {
	return containsAABVSAAB(s1, s2)
}
func containsAABVSPoint(s1, s2 Shape) bool {
	return intersectsAABVSPoint(s1, s2)
}
func containsAABVSLine(s1, s2 Shape) bool {
	return containsAABVSAAB(s1, s2)
}
func containsAABVSConvex(s1, s2 Shape) bool {
	return containsAABVSAAB(s1, s2)
}
func containsAABVSConcave(s1, s2 Shape) bool {
	return containsAABVSAAB(s1, s2)
}
func containsCircleVSAAB(s1, s2 Shape) bool {
	return containsCircleVSConvex(s1, s2)
}
func containsCircleVSCircle(s1, s2 Shape) bool {
	a, b := s1.BoundingBox(), s2.BoundingBox()
	aRSq := a.RadiusX() * a.RadiusY()
	bRSq := b.RadiusX() * b.RadiusY()
	if bRSq > aRSq {
		return false
	}
	return a.Centre().Minus(b.Centre()).LengthSq() <= aRSq-bRSq
}
func containsCircleVSPoint(s1, s2 Shape) bool {
	return intersectsCircleVSPoint(s1, s2)
}
func containsCircleVSLine(s1, s2 Shape) bool {
	return containsCircleVSConvex(s1, s2)
}
func containsCircleVSConvex(s1, s2 Shape) bool {
	i := s2.FirstVertexIndex()
	start := i
	box := s1.BoundingBox()
	cnt := box.Centre()
	rSq := box.RadiusX() * box.RadiusY()
	for {
		v := s2.Vertex(i)
		if v.Minus(cnt).LengthSq() > rSq {
			return false
		}
		i = s2.NextVertexIndexClockwise(i)
		if i == start {
			return true
		}
	}
}
func containsCircleVSConcave(s1, s2 Shape) bool {
	cci := s2.FirstConvexComponentIndex()
	ccistart := cci
	for {
		if !containsCircleVSConvex(s1, s2.ConvexComponent(cci)) {
			return false
		}
		cci = s2.NextConvexComponentIndex(cci)
		if cci == ccistart {
			break
		}
	}
	return true
}
func containsConvexVSAAB(s1, s2 Shape) bool {
	return containsConvexVSConvex(s1, s2)
}
func containsConvexVSCircle(s1, s2 Shape) bool {
	//does p contain c?
	psb, psbv, pst, pstv := leftVertices(s1)
	box := s2.BoundingBox()
	cnt := box.Centre()
	rad := box.RadiusX()
	if pstv.Y() < cnt.Y() {
		//circle is on top
		//check if c goes above the top side of p
		ai := pst
		a := pstv
		for {
			bi := s1.NextVertexIndexClockwise(ai)
			b := s1.Vertex(bi)
			if b.X() < a.X() || cnt.X()+rad < a.X() {
				//no more lines
				//or
				//circle is left of line, we're done
				return true
			}
			if cnt.X()-rad > b.X() || (cnt.Y()-rad < a.Y() && cnt.Y()-rad < b.Y()) {
				//circle is right of line, go to next line
				//or
				//circle is below line, go to next line
			} else if ClockwiseOfAB(cnt, b, a) || (NearestPointBetweenAB(cnt, a, b).Minus(cnt).LengthSq() < rad*rad) {
				//circle's center is above line
				//or
				//circle intersects line
				return false
			}
			ai, a = bi, b
		}
	} else {
		//circle is on bottom
		//check if c goes below the bottom side of p
		ai := psb
		a := psbv
		for {
			bi := s1.NextVertexIndexAnticlockwise(ai)
			b := s1.Vertex(bi)
			if b.X() < a.X() || cnt.X()+rad < a.X() {
				//no more lines
				//or
				//circle is left of line, we're done
				return true
			}
			if cnt.X()-rad > b.X() || (cnt.Y()-rad > a.Y() && cnt.Y()-rad > b.Y()) {
				//circle is right of line, go to next line
				//or
				//circle is above line, go to next line
			} else if ClockwiseOfAB(cnt, a, b) || (NearestPointBetweenAB(cnt, a, b).Minus(cnt).LengthSq() < rad*rad) {
				//circle's center is below line
				//or
				//circle intersects line
				return false
			}
			ai, a = bi, b
		}
	}
}
func containsConvexVSPoint(s1, s2 Shape) bool {
	return intersectsConvexVSConvex(s2, s1)
}
func containsConvexVSLine(s1, s2 Shape) bool {
	return containsConvexVSConvex(s1, s2)
}
func containsConvexVSConvex(s1, s2 Shape) bool {
	psb, psbv, pst, pstv := leftVertices(s1)
	p2sb, p2sbv, p2st, p2stv := leftVertices(s2)
	if psbv.X() > p2sbv.X() {
		//lines must start first and end last
		return false
	}
	//check if points from the top of p2 appear abo ve lines from the top of p
	ai := pst
	a := pstv
	bi := s1.NextVertexIndexClockwise(ai)
	b := s1.Vertex(bi)
	xi := p2st
	x := p2stv
	for {
		if x.X() < a.X() {
			//point is left of line, next point
			zi := s2.NextVertexIndexClockwise(xi)
			z := s2.Vertex(zi)
			if z.X() <= x.X() {
				//no more points
				break
			}
			xi, x = zi, z
			continue
		}
		if x.X() < b.X() {
			//point is to the left of the right of this line and so can be checked
			if ClockwiseOfAB(x, b, a) {
				return false
			}
			//point is under line, next point
			zi := s2.NextVertexIndexClockwise(xi)
			z := s2.Vertex(zi)
			if z.X() <= x.X() {
				//no more points
				break
			}
			xi, x = zi, z
			continue
		}
		//next line
		ai, a = bi, b
		bi = s1.NextVertexIndexClockwise(ai)
		b = s1.Vertex(bi)
		if b.X() <= a.X() {
			return false
			//no more lines, points must run out first
		}
	}
	//check if points from the bottom of p2 appear below lines from the bottom of p
	ai = psb
	a = psbv
	bi = s1.NextVertexIndexAnticlockwise(ai)
	b = s1.Vertex(bi)
	xi = p2sb
	x = p2sbv
	for {
		if x.X() < a.X() {
			zi := s2.NextVertexIndexAnticlockwise(xi)
			//point is left of line, next point
			z := s2.Vertex(zi)
			if z.X() <= x.X() {
				break
				//no more points
			}
			xi, x = zi, z
			continue
		}
		if x.X() < b.X() {
			//point is to the left of the right of this line and so can be checked
			if ClockwiseOfAB(x, a, b) {
				// fmt.Println("contains false")
				return false
			}
			//point is above line, next point
			zi := s2.NextVertexIndexAnticlockwise(xi)
			z := s2.Vertex(zi)
			if z.X() <= x.X() {
				//no more points
				break
			}
			xi, x = zi, z
			continue
		}
		//next line
		ai, a = bi, b
		bi = s1.NextVertexIndexAnticlockwise(ai)
		b = s1.Vertex(bi)
		if b.X() <= a.X() {
			//no more lines, points must run out first
			return false
		}
	}
	return true
}
func containsConvexVSConcave(s1, s2 Shape) bool {
	cci := s2.FirstConvexComponentIndex()
	ccistart := cci
	for {
		if !containsConvexVSConvex(s1, s2.ConvexComponent(cci)) {
			return false
		}
		cci = s2.NextConvexComponentIndex(cci)
		if cci == ccistart {
			break
		}
	}
	return true
}
