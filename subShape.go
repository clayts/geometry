package geometry

type SubShape struct {
	Shape
	first, last int //first to last, clockwise
}

type SubShaper interface {
	Shape
	Base() Shape
}

func NewSubShape(base Shape, firstVertexIndex, lastVertexIndex int) SubShape {
	return SubShape{base, firstVertexIndex, lastVertexIndex}
}

func NewCompleteSubShape(base Shape) SubShape {
	fvi := base.FirstVertexIndex()
	lvi := base.NextVertexIndexAnticlockwise(fvi)
	return SubShape{base, fvi, lvi}
}

func (p SubShape) ShapeType() ShapeType       { return ConvexType }
func (p *SubShape) setFirstVertexIndex(i int) { p.first = i }
func (p SubShape) Base() Shape                { return p.Shape }
func (p *SubShape) setBase(s Shape) {
	if b, ok := p.Base().(SubShaper); ok {
		fvi := b.FirstVertexIndex()
		lvi := b.NextVertexIndexAnticlockwise(fvi)
		newB := NewSubShape(b.Base(), fvi, lvi)
		newB.setBase(s)
		p.Shape = newB
	} else {
		p.Shape = s
	}
}
func (p *SubShape) setLastVertexIndex(i int) { p.last = i }
func (p SubShape) LastVertexIndex() int      { return p.last }

func (p SubShape) FirstVertexIndex() int { return p.first }
func (p SubShape) NextVertexIndexClockwise(i int) int {
	if i == p.last {
		return p.first
	}
	return p.Shape.NextVertexIndexClockwise(i)
}
func (p SubShape) NextVertexIndexAnticlockwise(i int) int {
	if i == p.first {
		return p.last
	}
	return p.Shape.NextVertexIndexAnticlockwise(i)
}
