package geometry

type Concave struct {
	Shape
	components []SubShape
}

func NewConcave(fs ...float32) Concave {
	c := Concave{NewConvex(fs...), nil}
	c.recalculateComponents()
	return c
}

func NewConcaveFromVectors(vs ...Vector) Concave {
	c := Concave{NewConvexFromVectors(vs...), nil}
	c.recalculateComponents()
	return c
}

//Shape Interface
func (cc Concave) ConvexComponent(i int) SubShape { return cc.components[i] }
func (cc Concave) FirstConvexComponentIndex() int { return 0 }
func (cc Concave) NextConvexComponentIndex(i int) int {
	if i == len(cc.components)-1 {
		return 0
	}
	return i + 1
}
func (cc Concave) ShapeType() ShapeType { return ConcaveType }

// func (cc Concave) Copy() Shape {
// 	cc2 := Concave{}
// 	cc2.Shape = cc.Shape.Copy()
// 	cc2.components = make([]SubShape, len(cc.components))
// 	for i, sub := range cc.components {
// 		sub2 := SubShape{}
// 		sub2.Shape = cc
// 		sub2.first = sub.first
// 		sub2.last = sub.last
// 		cc2.components[i] = sub2
// 	}
// 	return cc2
// }
func (cc *Concave) recalculateComponents() {
	cc.components = convexComponents(cc.Shape)
}

func firstConcaveVertexIndex(p Shape) int { //returns -1 if none
	ai := p.FirstVertexIndex()
	a := p.Vertex(ai)
	bi := p.NextVertexIndexClockwise(ai)
	if bi == ai {
		//single vertex polygonal is always convex
		return -1
	}
	start := bi
	b := p.Vertex(bi)
	for {
		ci := p.NextVertexIndexClockwise(bi)
		c := p.Vertex(ci)
		if !ClockwiseOfAB(c, a, b) {
			//point is not convex
			return bi
		}
		if ci == start {
			//no more points
			return -1
		}
		a, bi, b = b, ci, c
	}
}

//
// func convexComponentsWithOriginal(p Shape) []Shape {
// 	answer := convexComponents(p)
// 	if answer == nil {
// 		return []Shape{p}
// 	}
// 	return answer
// }

func convexComponents(p Shape) []SubShape {
	ci := firstConcaveVertexIndex(p)
	if ci == -1 {
		fvi := p.FirstVertexIndex()
		return []SubShape{SubShape{p, fvi, p.NextVertexIndexAnticlockwise(fvi)}}
	}
	var answer []SubShape
	bi := p.NextVertexIndexAnticlockwise(ci)
	// if bi == ci {
	// 	f(p)
	// 	return
	// }
	ai := p.NextVertexIndexAnticlockwise(bi)
	// if ai == ci {
	// 	f(p)
	// 	return
	// }
	ear := SubShape{p, ai, ci}
	clipped := SubShape{p, ci, ai}
	c := p.Vertex(ci)
	b := p.Vertex(bi)
	a := p.Vertex(ai)
	for !ClockwiseOfAB(c, a, b) || containsAVertex(ear, clipped) {
		ci, c, bi, b = bi, b, ai, a
		ai = p.NextVertexIndexAnticlockwise(ai)
		a = p.Vertex(ai)
		ear.setFirstVertexIndex(ai)
		ear.setLastVertexIndex(ci)
		clipped.setFirstVertexIndex(ci)
		clipped.setLastVertexIndex(ai)
	}
	//ear is legit and must exist
	if firstConcaveVertexIndex(clipped) == -1 {
		return append(answer, ear, clipped)
	}

	di := p.NextVertexIndexClockwise(ci)
	for {
		zi := p.NextVertexIndexAnticlockwise(ai)
		if zi == di {
			break
		}
		ear.setFirstVertexIndex(zi)
		clipped.setLastVertexIndex(zi)
		if vertexIsConvex(ear, zi) && !containsAVertex(ear, clipped) {
			if vertexIsConvex(clipped, ci) {
				answer = append(answer, ear)
				return append(answer, convexComponents(clipped)...)
			}
			ai = zi
			continue
		} else {
			ear.setFirstVertexIndex(ai)
			clipped.setLastVertexIndex(ai)
			break
		}
	}
	answer = append(answer, ear)
	return append(answer, convexComponents(clipped)...)
}

func vertexIsConvex(p Shape, i int) bool {
	return ClockwiseOfAB(p.Vertex(p.NextVertexIndexClockwise(i)), p.Vertex(p.NextVertexIndexAnticlockwise(i)), p.Vertex(i))
}

func containsAVertex(s1, s2 Shape) bool {
	i := s2.FirstVertexIndex()
	start := i

	for {
		v := s2.Vertex(i)
		if Contains(s1, v) {
			return true
		}
		i = s2.NextVertexIndexClockwise(i)
		if i == start {
			return false
		}
	}
}
