package geometry

import "math"

type RegularPolygon struct {
	Circle
	n int
}

func NewRegularPolygon(c Circle, n int) RegularPolygon {
	return RegularPolygon{c, n}
}

func (rp RegularPolygon) NextVertexIndexAnticlockwise(i int) int {
	if i == 0 {
		return rp.n - 1
	}
	return i - 1
}
func (rp RegularPolygon) NextVertexIndexClockwise(i int) int {
	if i == rp.n-1 {
		return 0
	}
	return i + 1
}
func (rp RegularPolygon) ShapeType() ShapeType { return ConvexType }
func (rp RegularPolygon) Vertex(i int) Vector {
	return VertexOfCircle(rp.Circle, rp.Rotation()+((float32(i)/float32(rp.n))*math.Pi*2))
}
