package geometry

import "math"

// func (rp RegularPolygon)

const CircleVertexCount = 64

func VertexOfCircle(c Circle, radians float32) Vector {
	sin, cos := sincos(((math.Pi) - radians))
	v := Vector{c.Centre().X() + c.Radius()*cos, c.Centre().Y() + c.Radius()*sin}
	return v
}

type Circle struct{ vs [4]float32 } //centre x,y, radius, rotation
func NewCircleFromCentreRadiusAngle(centre Vector, radius, angle float32) Circle {
	return NewCircle(centre.X(), centre.Y(), radius, angle)
}
func NewCircle(cx, cy, r, a float32) Circle {
	return Circle{[4]float32{cx, cy, r, a}}
}

func (c Circle) Centre() Vector    { return Vector{c.CentreX(), c.CentreY()} }
func (c Circle) CentreX() float32  { return c.vs[0] }
func (c Circle) CentreY() float32  { return c.vs[1] }
func (c Circle) Radius() float32   { return c.vs[2] }
func (c Circle) Rotation() float32 { return c.vs[3] }
func (c Circle) setRotation(radians float32) {
	for radians > mathPi*2 {
		radians -= mathPi * 2
	}
	c.vs[3] = radians
}
func (c *Circle) setCentre(v Vector)       { c.vs[0], c.vs[1] = v.X(), v.Y() }
func (c *Circle) setCentreXY(x, y float32) { c.vs[0], c.vs[1] = x, y }
func (c *Circle) setCentreX(x float32)     { c.vs[0] = x }
func (c *Circle) setCentreY(y float32)     { c.vs[1] = y }
func (c *Circle) setRadius(r float32)      { c.vs[2] = r }

// func (c *Circle) Rotate(radians float32) {
// 	c.setRotation(c.Rotation() + radians)
// 	c.setCentre(c.Centre().Rotated(radians))
// }
func (c *Circle) Translate(t Vector) {
	c.setCentre(t.Plus(c.Centre()))
}

//Shape interface
func (c Circle) BoundingBox() AAB { return NewAABFromCentreRxRy(c.Centre(), c.Radius(), c.Radius()) }

// func (c Circle) Copy() Shape                      { return Circle{c.CentreX(), c.CentreY(), c.Radius()} }
func (c Circle) ConvexComponent(int) SubShape     { return NewCompleteSubShape(c) }
func (c Circle) FirstConvexComponentIndex() int   { return 0 }
func (c Circle) NextConvexComponentIndex(int) int { return 0 }
func (c Circle) FirstVertexIndex() int            { return 0 }
func (c Circle) NextVertexIndexAnticlockwise(i int) int {
	if i == 0 {
		return CircleVertexCount - 1
	}
	return i - 1
}
func (c Circle) NextVertexIndexClockwise(i int) int {
	if i == CircleVertexCount-1 {
		return 0
	}
	return i + 1
}
func (c Circle) ShapeType() ShapeType { return CircleType }
func (c Circle) Vertex(i int) Vector {
	return VertexOfCircle(c, c.Rotation()+((float32(i)/CircleVertexCount)*math.Pi*2))
}
