package geometry

import "math"

const mathPi = float32(math.Pi)

//in radians
func sincos(f float32) (float32, float32) {
	sin, cos := math.Sincos(float64(f))
	return float32(sin), float32(cos)
}

func Sqrt(f float32) float32 {
	return float32(math.Sqrt(float64(f)))
}

func Clockwise(v Vector, l Line) bool {
	return ClockwiseOfAB(v, l.Start(), l.End())
}

func ClockwiseOfAB(p, a, b Vector) bool {
	return b.Minus(a).Cross(p.Minus(a)) < 0
}

func NearestPointOnLine(p Vector, l Line) Vector {
	return NearestPointBetweenAB(p, l.Start(), l.End())
}

func NearestPointBetweenAB(p, a, b Vector) Vector {
	line := a.Minus(b)
	lengthSq := line.LengthSq()
	t := p.Minus(a).Dot(line) / lengthSq
	if t > 1 {
		t = 1
	} else if t < 0 {
		t = 0
	}
	proj := a.Plus(line.TimesXY(t))
	return p.Minus(proj)
}

var Sqrt3 = float32(math.Sqrt(3))

func ThirdPointOfEquilateral(a, b Vector) Vector {
	//	X=x1+x2± √3(y1−y2) over 2, Y=y1+y2± √3(x1−x2) over 2
	return Vector{(a.X() + b.X() + (Sqrt3 * (a.Y() - b.Y()))) / 2, (a.Y() + b.Y() - (Sqrt3 * (a.X() - b.X()))) / 2}
}
