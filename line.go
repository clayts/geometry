package geometry

type Line struct{ vs [4]float32 } //startx, starty, endx, endy
func NewLineFromStartEnd(start, end Vector) Line {
	return NewLine(start.X(), start.Y(), end.X(), end.Y())
}
func NewLine(sx, sy, ex, ey float32) Line { return Line{[4]float32{sx, sy, ex, ey}} }
func (l Line) Start() Vector              { return Vector{l.StartX(), l.StartY()} }
func (l Line) StartX() float32            { return l.vs[0] }
func (l Line) StartY() float32            { return l.vs[1] }
func (l Line) End() Vector                { return Vector{l.EndX(), l.EndY()} }
func (l Line) EndX() float32              { return l.vs[2] }
func (l Line) EndY() float32              { return l.vs[3] }
func (l *Line) setStart(v Vector)         { l.vs[0], l.vs[1] = v.X(), v.Y() }
func (l *Line) setStartX(f float32)       { l.vs[0] = f }
func (l *Line) setStartY(f float32)       { l.vs[1] = f }
func (l *Line) setEnd(v Vector)           { l.vs[2], l.vs[3] = v.X(), v.Y() }
func (l *Line) setEndX(f float32)         { l.vs[2] = f }
func (l *Line) setEndY(f float32)         { l.vs[3] = f }
func (l *Line) Rotate(radians float32) {
	l.setStart(l.Start().Rotated(radians))
	l.setEnd(l.End().Rotated(radians))
}
func (l *Line) Translate(t Vector) {
	l.setStart(t.Plus(l.Start()))
	l.setEnd(t.Plus(l.End()))
}
func (l Line) LengthSq() float32 { return l.End().Minus(l.Start()).LengthSq() }
func (l Line) Length() float32   { return l.End().Minus(l.Start()).Length() }
func (l Line) Angle() float32    { return l.End().Minus(l.Start()).Angle() }

//Shape Interface
// func (l Line) Copy() Shape                      { return Line{l[0], l[1], l[2], l[3]} }
func (l Line) BoundingBox() AAB                 { return CalculateBoundingBoxOfPolygon(l) }
func (l Line) ConvexComponent(int) SubShape     { return NewCompleteSubShape(l) }
func (l Line) FirstConvexComponentIndex() int   { return 0 }
func (l Line) NextConvexComponentIndex(int) int { return 0 }
func (l Line) FirstVertexIndex() int            { return 0 }
func (l Line) NextVertexIndexAnticlockwise(i int) int {
	if i == 0 {
		return 1
	}
	return 0
}
func (l Line) NextVertexIndexClockwise(i int) int {
	if i == 0 {
		return 1
	}
	return 0
}
func (l Line) ShapeType() ShapeType { return LineType }
func (l Line) Vertex(i int) Vector {
	if i == 0 {
		return l.Start()
	}
	return l.End()
}
