package geometry

// type AABer interface {
// 	BoundingBox() AAB
// }

type Side int

var AABZero = NewAABFromLBRT(0, 0, 0, 0)
var AABUnit = NewAABFromLBRT(0, 0, 1, 1)
var AABUnitAtOrigin = NewAABFromLBRT(-0.5, -0.5, 0.5, 0.5)
var AABRadiusOne = NewAAB(0, 0, 1, 1)

const (
	L Side = iota
	B
	R
	T
)

type Corner int

const (
	LB Corner = iota
	LT
	RT
	RB
)

func (s Side) ToCorner(s2 Side) Corner {
	if s == L {
		if s2 == B {
			return LB
		}
		return LT
	}
	if s2 == B {
		return RB
	}
	return RT
}

func (c Corner) ToSides() (Side, Side) {
	switch c {
	case LB:
		return L, B
	case LT:
		return L, T
	case RT:
		return R, T
	default:
		return R, B
	}
}

type AAB struct{ vs [4]float32 } //centre x,y radius x,y

func NewAAB(x, y, rx, ry float32) AAB {
	return AAB{[4]float32{x, y, rx, ry}}
}
func NewAABFromLBRT(l, b, r, t float32) AAB {
	rx := (r - l) / 2
	ry := (t - b) / 2
	cx := l + rx
	cy := b + ry
	return NewAAB(cx, cy, rx, ry)
}
func NewAABFromCentreSize(centre, size Vector) AAB {
	return NewAAB(centre.X(), centre.Y(), size.X()/2, size.Y()/2)
}
func NewAABFromCornerWidthHeight(cornerLocation Vector, corner Corner, width, height float32) AAB {
	if corner == LB {
		return NewAABFromLBRT(cornerLocation.X(), cornerLocation.Y(), cornerLocation.X()+width, cornerLocation.Y()+height)
	} else if corner == LT {
		return NewAABFromLBRT(cornerLocation.X(), cornerLocation.Y()-height, cornerLocation.X()+width, cornerLocation.Y())
	} else if corner == RT {
		return NewAABFromLBRT(cornerLocation.X()-width, cornerLocation.Y()-height, cornerLocation.X(), cornerLocation.Y())
	} else {
		//RB
		return NewAABFromLBRT(cornerLocation.X()-width, cornerLocation.Y(), cornerLocation.X(), cornerLocation.Y()+height)
	}
}
func NewAABFromXYWH(x, y, w, h float32) AAB {
	return NewAAB(x, y, w/2, h/2)
}
func NewAABFromCentreRadius(centre, radius Vector) AAB {
	return NewAAB(centre.X(), centre.Y(), radius.X(), radius.Y())
}
func NewAABFromCentreRxRy(centre Vector, rx, ry float32) AAB {
	return NewAAB(centre.X(), centre.Y(), rx, ry)
}
func NewAABFromXYRadius(x, y float32, radius Vector) AAB {
	return NewAAB(x, y, radius.X(), radius.Y())
}
func (a AAB) MinimumRadius() float32 {
	mv := a.RadiusX()
	if mvt := a.RadiusY(); mvt < mv {
		mv = mvt
	}
	return mv
}
func (a AAB) MaximumRadius() float32 {
	mv := a.RadiusX()
	if mvt := a.RadiusY(); mvt > mv {
		mv = mvt
	}
	return mv
}

func (a AAB) TimesWidth(f float32) AAB {
	return NewAABFromCentreRxRy(a.Centre(), a.RadiusX()*f, a.RadiusY())
}

func (a AAB) TimesHeight(f float32) AAB {
	return NewAABFromCentreRxRy(a.Centre(), a.RadiusX(), a.RadiusY()*f)
}

func (a AAB) Centre() Vector       { return Vector{a.vs[0], a.vs[1]} }
func (a AAB) CentreX() float32     { return a.vs[0] }
func (a AAB) CentreY() float32     { return a.vs[1] }
func (a AAB) Radius() Vector       { return Vector{a.vs[2], a.vs[3]} }
func (a AAB) RadiusX() float32     { return a.vs[2] }
func (a AAB) RadiusY() float32     { return a.vs[3] }
func (a AAB) Size() Vector         { return a.Radius().TimesXY(2) }
func (a AAB) Width() float32       { return a.RadiusX() * 2 }
func (a AAB) Height() float32      { return a.RadiusY() * 2 }
func (a *AAB) setSize(v Vector)    { a.setRadius(v.OverXY(2)) }
func (a *AAB) setWidth(f float32)  { a.setRadiusX(f / 2) }
func (a *AAB) setHeight(f float32) { a.setRadiusY(f / 2) }
func (a AAB) L() float32           { return a.CentreX() - a.RadiusX() }
func (a AAB) B() float32           { return a.CentreY() - a.RadiusY() }
func (a AAB) R() float32           { return a.CentreX() + a.RadiusX() }
func (a AAB) T() float32           { return a.CentreY() + a.RadiusY() }
func (a AAB) LB() Vector           { return a.Centre().Minus(a.Radius()) }
func (a AAB) LT() Vector           { return a.Centre().MinusXPlusY(a.Radius()) }
func (a AAB) RT() Vector           { return a.Centre().Plus(a.Radius()) }
func (a AAB) RB() Vector           { return a.Centre().PlusXMinusY(a.Radius()) }
func (a AAB) Side(s Side) float32 {
	switch s {
	case L:
		return a.L()
	case B:
		return a.B()
	case R:
		return a.R()
	default:
		return a.T()
	}
}
func (a AAB) Corner(c Corner) Vector {
	switch c {
	case LB:
		return a.LB()
	case LT:
		return a.LT()
	case RT:
		return a.RT()
	default:
		return a.RB()
	}
}
func (a *AAB) setCentre(v Vector)   { a.vs[0], a.vs[1] = v[0], v[1] }
func (a *AAB) setCentreX(f float32) { a.vs[0] = f }
func (a *AAB) setCentreY(f float32) { a.vs[1] = f }
func (a *AAB) setRadius(v Vector)   { a.vs[2], a.vs[3] = v[0], v[1] }
func (a *AAB) setRadiusX(f float32) { a.vs[2] = f }
func (a *AAB) setRadiusY(f float32) { a.vs[3] = f }
func (a *AAB) setL(f float32) {
	rx := (a.R() - f) / 2
	cx := f + rx
	a.vs[0], a.vs[2] = cx, rx
}
func (a *AAB) setB(f float32) {
	ry := (a.T() - f) / 2
	cy := f + ry
	a.vs[1], a.vs[3] = cy, ry
}
func (a *AAB) setR(f float32) {
	rx := (f - a.L()) / 2
	cx := f - rx
	a.vs[0], a.vs[2] = cx, rx
}
func (a *AAB) setT(f float32) {
	ry := (f - a.B()) / 2
	cy := f - ry
	a.vs[1], a.vs[3] = cy, ry
}
func (a *AAB) setLB(v Vector) {
	a.setL(v.X())
	a.setB(v.Y())
}
func (a *AAB) setLT(v Vector) {
	a.setL(v.X())
	a.setT(v.Y())
}
func (a *AAB) setRT(v Vector) {
	a.setR(v.X())
	a.setT(v.Y())
}
func (a *AAB) setRB(v Vector) {
	a.setR(v.X())
	a.setB(v.Y())
}
func (a *AAB) setSide(s Side, f float32) {
	switch s {
	case L:
		a.setL(f)
	case B:
		a.setB(f)
	case R:
		a.setR(f)
	default:
		a.setT(f)
	}
}
func (a *AAB) setCorner(c Corner, v Vector) {
	switch c {
	case LB:
		a.setLB(v)
	case LT:
		a.setLT(v)
	case RT:
		a.setRT(v)
	default:
		a.setRB(v)
	}
}
func (a *AAB) Rotate(radians float32) {
	a.setCentre(a.Centre().Rotated(radians))
}
func (a *AAB) Translate(t Vector) {
	a.setCentre(a.Centre().Plus(t))
}

//Shape interface
func (a AAB) BoundingBox() AAB { return a }

// func (a AAB) Copy() Shape                      { return AAB{a.CentreX(), a.CentreY(), a.RadiusX(), a.RadiusY()} }
func (a AAB) ConvexComponent(int) SubShape     { return NewCompleteSubShape(a) }
func (a AAB) FirstConvexComponentIndex() int   { return 0 }
func (a AAB) NextConvexComponentIndex(int) int { return 0 }
func (a AAB) FirstVertexIndex() int            { return 0 }
func (a AAB) NextVertexIndexAnticlockwise(i int) int {
	if i == 0 {
		return 3
	}
	return i - 1
}
func (a AAB) NextVertexIndexClockwise(i int) int {
	if i == 3 {
		return 0
	}
	return i + 1
}
func (a AAB) ShapeType() ShapeType { return AABType }
func (a AAB) Vertex(i int) Vector {
	return a.Corner(Corner(i))
}
