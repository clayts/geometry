package geometry

type Transform struct {
	x, y, t Vector
}

var IdentityTransform = NewTransform(1, 0, 0, 1, 0, 0)

func NewTransform(a, b, c, d, tx, ty float32) Transform {
	return Transform{Vector{a, b}, Vector{c, d}, Vector{tx, ty}}
}

func NewAABToAABTransform(from, to AAB) Transform {
	scaleX := to.RadiusX() / from.RadiusX()
	scaleY := to.RadiusY() / from.RadiusY()
	return NewTranslationTransform(
		from.Centre().Opposite(),
	).
		FollowedBy(
			NewScaleTransform(
				Vector{scaleX, scaleY},
			),
		).FollowedBy(
		NewTranslationTransform(
			to.Centre(),
		),
	)
}

func NewTransformFromSinCosTranslation(sin, cos float32, translation Vector) Transform {
	t := NewTransformFromSinCos(sin, cos)
	t.t = translation
	return t
}

func NewTransformFromSinCos(sin, cos float32) Transform {
	t := Transform{}
	t.x = Vector{cos, sin}
	t.y = Vector{-sin, cos}
	return t
}

func NewTransposedTransform(a, c, tx, b, d, ty float32) Transform {
	return NewTransform(a, b, c, d, tx, ty)
}

func NewTranslationTransform(translation Vector) Transform {
	return NewTransposedTransform(1, 0, translation.X(),
		0, 1, translation.Y())
}

func NewRotationTransform(radians float32) Transform {
	s, c := sincos(radians)
	return NewTransposedTransform(c, -s, 0, s, c, 0)
}

func NewScaleTransform(scale Vector) Transform {
	// fmt.Println("new scale transform:")
	t := NewTransposedTransform(scale.X(), 0, 0, 0, scale.Y(), 0)
	// fmt.Println(t)
	return t
}

func (t Transform) Inverse() Transform {
	invDet := 1 / ((t.x.X() * t.y.Y()) - (t.y.X() * t.x.Y()))

	return NewTransposedTransform(
		t.y.Y()*invDet, -t.y.X()*invDet,
		((t.y.X()*t.t.Y())-(t.t.X()*t.y.Y()))*invDet,
		-t.x.Y()*invDet,
		t.x.X()*invDet,
		((t.t.X()*t.x.Y())-(t.x.X()*t.t.Y()))*invDet,
	)
}

func (t Transform) FollowedBy(t2 Transform) Transform {
	return t2.Times(t)
}

func (t Transform) After(t2 Transform) Transform {
	return t.Times(t2)
}

func (t Transform) Times(t2 Transform) Transform {
	return NewTransposedTransform(
		t.x.X()*t2.x.X()+t.y.X()*t2.x.Y(),
		t.x.X()*t2.y.X()+t.y.X()*t2.y.Y(),
		t.x.X()*t2.t.X()+t.y.X()*t2.t.Y()+t.t.X(), //TODO x.y  y.x SWAP!??!?
		t.x.Y()*t2.x.X()+t.y.Y()*t2.x.Y(),
		t.x.Y()*t2.y.X()+t.y.Y()*t2.y.Y(),
		t.x.Y()*t2.t.X()+t.y.Y()*t2.t.Y()+t.t.Y(), //TODO x.y  y.x SWAP!??!?
	)
}

func (t Transform) Transformed(v Vector) Vector {
	// return t.Translated(t.Oriented(v))
	return NewVector(
		(t.x.X()*v.X())+(t.y.X()*v.Y())+t.t.X(), //TODO x.y y.x swap??
		(t.x.Y()*v.X())+(t.y.Y()*v.Y())+t.t.Y(), //TODO x.y y.x swap??
	)
}

func (t Transform) Oriented(v Vector) Vector {
	return NewVector(
		(t.x.X()*v.X())+(t.y.X()*v.Y()), //TODO x.y y.x swap??
		(t.x.Y()*v.X())+(t.y.Y()*v.Y()), //TODO x.y y.x swap??
	)
}

func (t Transform) Translated(v Vector) Vector {
	return v.Plus(t.t)
}

func (t Transform) Scale() Vector {
	// return NewVector(t.y.Length(), t.x.Length())
	return NewVector(t.y.Length(), t.x.Length())
}

func (t Transform) IncludesRotation() bool {
	return t.x.Y() != 0 || t.y.X() != 0 || t.x.X() < 0 //TODO or y.x < 0?
}

func (t Transform) XBasis() Vector        { return t.x }
func (t Transform) YBasis() Vector        { return t.y }
func (t Transform) Translation() Vector   { return t.t }
func (t Transform) XBasisX() float32      { return t.x.X() }
func (t Transform) YBasisX() float32      { return t.y.X() }
func (t Transform) TranslationX() float32 { return t.t.X() }
func (t Transform) XBasisY() float32      { return t.x.Y() }
func (t Transform) YBasisY() float32      { return t.y.Y() }
func (t Transform) TranslationY() float32 { return t.t.Y() }
